import renderJokePage from '../views/joke-page.js';
import getJoke from '../services/jokes-api.js';
import loader from '../views/loader.js';

export default async function () {

    loader();

    const jokeType = window.location.hash.split('=')[1];

    const jokeToRender = {
        category: 'Programing',
        jokeText: '',
        delivery: ''
    };

    try {
        const joke = await getJoke(jokeType);
        jokeToRender.category = joke.category;

        if (joke.type === 'single') {
            jokeToRender.jokeText = joke.joke;

        } else {
            jokeToRender.jokeText = joke.setup;
            jokeToRender.delivery = joke.delivery;

        }

        renderJokePage(jokeToRender);


    } catch (err) {
        console.log('ERROR: ' + err.message);

    }


}