
import homepageController from './controllers/home-controller.js'
import secondPageController from './controllers/joke-controller.js'

export default {
    homepage: {
        hash: '#home',
        init: homepageController
    },
    secondPage: {
        hash: '#joke',
        init: secondPageController
    }

}