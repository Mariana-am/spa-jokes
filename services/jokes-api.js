


export default async function getJoke(jokeType) {

    const apiURL = 'https://sv443.net/jokeapi/v2/joke/';

    try {
        const response = await fetch(`${apiURL}${jokeType}`);
        const jokeObj = await response.json();

        return jokeObj;

    } catch (err) {
        console.log(err.message);
        return err.message;

    }

}