
export default function renderBanner() {

    var img = $('<img>', {
        id: 'banner-img',
        class: 'img-fluid mt-4',
        src: 'https://i.picsum.photos/id/52/800/200.jpg'
    });

    return img;
}