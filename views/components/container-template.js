

export function containerTemplate() {

    const divColumns = $('<div>', { class: 'col-md-12', id: 'divColumns' });

    const divRow = $('<div>', { class: 'row' })
        .append(divColumns);

    const containerDiv = $('<div>', { class: 'container' })
        .append(divRow);

    return containerDiv;
}