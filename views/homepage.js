import banner from './components/banner.js'
import { containerTemplate } from './components/container-template.js'

export function jokeOptions() {

    //improve this part to have the options created from API info    

    const divWithOptions = $('<div>', { class: 'mt-4' });
    ['Programming', 'Dark', 'Miscellaneous', 'Any'].forEach(element => {
        let btn = $('<button>', { class: 'btn btn-info m-1' });
        btn.text(element);
        btn.click(() => set_hash(element, '#joke'));
        divWithOptions.append(btn);
    });

    divWithOptions.append($('<div>', { text: 'Exclude joke with the following flags: ' }));


    ['nsfw', 'religious', 'political', 'racist', 'sexist'].forEach(element => {
        const checkbox = $('<div>', { class: 'custom-control custom-checkbox' })
            .append($('<input>', { type: 'checkbox', class: 'custom-control-input', id: 'defaultUnchecked' }))
            .append($('<label>', { class: 'custom-control-lable', for: 'defaultUnchecked', id: 'checkbox', text: element }));
        divWithOptions.append(checkbox);
    });

    const noJoke = $('<h1>', { class: 'text-center', text: 'No Joke' });


    $('#app')
        .empty()
        .append(containerTemplate);

    $('#divColumns')
        .append(banner)
        .append(divWithOptions)
        .append(noJoke);


    function set_hash(jokeType, new_hash) {
        window.location.hash = `${new_hash}?type=${jokeType}`;
    }

}