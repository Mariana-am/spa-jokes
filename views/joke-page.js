import { containerTemplate } from './components/container-template.js';
import banner from './components/banner.js';

export default function renderJokePage({ category, jokeText, delivery }) {



    const jokeContent = $('<div>', { class: 'mt-4' })
        .append($('<p>').text(category))
        .append($('<p>').text(jokeText))
        .append($('<p>').text(delivery));

    const goBackBtn = $('<a href="https://elegant-ardinghelli-1dcaaa.netlify.app/">')
        //const goBackBtn = $('<a href="http://127.0.0.1:5500/">')
        .append($('<button>', { class: 'btn btn-secondary mr-2', text: 'Return' }));

    const anotherJokeBtn = $('<a href="http://127.0.0.1:5500/">')
        .append($('<button>', { class: 'btn btn-secondary', text: 'Another joke' }));


    const contentDiv = $('<div>')
        .append(banner)
        .append(jokeContent)
        .append(goBackBtn)
        .append(anotherJokeBtn);

    $('#app')
        .empty()
        .append(containerTemplate);

    $('#divColumns')
        .append(contentDiv);

}