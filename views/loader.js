
import { containerTemplate } from './components/container-template.js'
import banner from './components/banner.js'

export default (() => {


    $('#app')
        .empty()
        .append(containerTemplate);

    $('#divColumns')
        .append(banner)
        .append($('<div>', { class: 'loader ', id: 'loader' }));

});
